package vince.games.chifoumi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vince.games.chifoumi.enums.Gesture;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestRound {
    private Player player1;
    private Player player2;
    private Round round;

    @BeforeEach
    public void player_setup() {
        player1 = new Player("Alice");
        player2 = new Player("Bob");
        round = new Round(player1, player2);
    }


    @Test
    public void player1_wins() {
        player1.setGesture(Gesture.paper);
        player2.setGesture(Gesture.stone);
        Assertions.assertEquals(player1, round.getWinner());
    }

    @Test
    public void player2_wins() {
        player1.setGesture(Gesture.scissors);
        player2.setGesture(Gesture.stone);
        Assertions.assertEquals(player2, round.getWinner());
    }

    @Test
    public void nobody_wins() {
        player1.setGesture(Gesture.stone);
        player2.setGesture(Gesture.stone);
        Assertions.assertEquals(Player.nobody, round.getWinner());
    }

    @Test
    public void nullPointer() {
        NullPointerException thrown = assertThrows(
                NullPointerException.class,
                () -> round.getWinner(),
                "Expected getWinner() to throw, but it didn't"
        );
    }

    @Test
    public void nullPointer2() {
        player1.setGesture(Gesture.stone);
        NullPointerException thrown = assertThrows(
                NullPointerException.class,
                () -> round.getWinner(),
                "Expected getWinner() to throw, but it didn't"
        );
    }

    @Test
    public void nullPointer3() {
        player2.setGesture(Gesture.stone);
        NullPointerException thrown = assertThrows(
                NullPointerException.class,
                () -> round.getWinner(),
                "Expected getWinner() to throw, but it didn't"
        );
    }

}
