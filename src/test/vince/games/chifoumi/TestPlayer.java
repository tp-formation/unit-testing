package vince.games.chifoumi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import vince.games.chifoumi.Player;
import vince.games.chifoumi.enums.Gesture;

public class TestPlayer {
    @Test
    public void player_creation() {
        Player p = new Player("Vincent");
        Assertions.assertNull(p.getCurrentGesture());
        Assertions.assertTrue(p.displayName().compareTo("Player Vincent") == 0);
    }

    @Test
    public void player_nobody() {
        Assertions.assertTrue(Player.nobody.displayName().compareTo("Nobody") == 0);
    }

    @Test
    public void player_gesture() {
        Player p = new Player("Vincent");
        p.setGesture(Gesture.paper);
        Assertions.assertTrue(p.getCurrentGesture() == Gesture.paper);
    }

}
