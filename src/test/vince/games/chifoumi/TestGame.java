package vince.games.chifoumi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import vince.games.chifoumi.enums.Gesture;
import vince.games.chifoumi.exceptions.GameEndedException;
import vince.games.chifoumi.exceptions.GameNotEndedException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestGame {
    Player p1 = new Player("Alice");
    Player p2 = new Player("Bob");

    @Test
    public void gameNotEnded() {
        Game game = new Game(p1, p2, 1);
        assertThrows(
                GameNotEndedException.class,
                () -> game.getWinner(),
                "Expected getWinner() to throw, but it didn't");
    }

    @Test
    public void gameEnded() {
        Game game = new Game(p1, p2, 0);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.stone);
        assertThrows(
                GameEndedException.class,
                () -> game.playRound(),
                "Expected playRound() to throw, but it didn't");
    }

    @Test
    public void threeRoundsP1Wins() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.stone);
        game.playRound();
        game.playRound();
        game.playRound();
        Assertions.assertEquals(p1, game.getWinner());
    }

    @Test
    public void threeRoundsP2Wins() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.scissors);
        game.playRound();
        game.playRound();
        game.playRound();
        Assertions.assertEquals(p2, game.getWinner());
    }

    @Test
    public void threeRoundsNobodyWins() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.paper);
        game.playRound();
        game.playRound();
        game.playRound();
        Assertions.assertEquals(Player.nobody, game.getWinner());
    }

    @Test
    public void roundNumber() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.stone);
        game.playRound();
        Assertions.assertEquals("Round 1", game.getCurrentRoundNumber());
    }

    @Test
    public void roundScore() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.stone);
        game.playRound();
        Assertions.assertEquals("Player Alice : 1 points / Player Bob : -1 points",
                game.getCurrentScore());
    }

    @Test
    public void roundRecord() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.stone);
        game.playRound();
        Assertions.assertEquals("Player Alice : Paper / Player Bob : Stone",
                game.getRoundRecord());
    }

    @Test
    public void threeRoundsWinLooseWinLog() throws GameEndedException, GameNotEndedException {
        Game game = new Game(p1, p2, 3);
        p1.setGesture(Gesture.paper);
        p2.setGesture(Gesture.stone);
        game.playRound();
        p1.setGesture(Gesture.scissors);
        p2.setGesture(Gesture.stone);
        game.playRound();
        p1.setGesture(Gesture.scissors);
        p2.setGesture(Gesture.paper);
        game.playRound();

        Assertions.assertArrayEquals(new String[]{
                "Round 1",
                "Player Alice : Paper / Player Bob : Stone",
                "Player Alice : 1 points / Player Bob : -1 points",
                "Round 2",
                "Player Alice : Scissors / Player Bob : Stone",
                "Player Alice : 0 points / Player Bob : 0 points",
                "Round 3",
                "Player Alice : Scissors / Player Bob : Paper",
                "Player Alice : 1 points / Player Bob : -1 points",
                "Player Alice wins"
        }, game.getGameRecord().toArray());
    }

}
