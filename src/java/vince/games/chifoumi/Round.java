package vince.games.chifoumi;

import vince.games.chifoumi.enums.Gesture;
import vince.games.chifoumi.enums.Result;
import vince.games.chifoumi.exceptions.GameEndedException;

public class Round {
    private Player player1;
    private Player player2;


    public Round(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public Player getWinner() {
        Result result = versus(player1.getCurrentGesture(), player2.getCurrentGesture());
        if (result == Result.win) {
            return this.player1;
        } else if (result == Result.loose) {
            return this.player2;
        } else return Player.nobody;

    }

    /**
     * @param gesture1
     * @param gesture2
     * @return win if gesture1 > gesture2, loose if gesture1 < gesture2, equality if nought
     */
    public Result versus(Gesture gesture1, Gesture gesture2) {
        if (gesture1 == gesture2)
            return Result.equality;
        if ((gesture1 == Gesture.paper && gesture2 == Gesture.stone) ||
                (gesture1 == Gesture.scissors && gesture2 == Gesture.paper) ||
                (gesture1 == Gesture.stone && gesture2 == Gesture.scissors))
            return Result.win;
        else
            return
                    Result.loose;

    }

}
