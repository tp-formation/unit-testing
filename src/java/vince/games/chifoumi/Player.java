package vince.games.chifoumi;

import vince.games.chifoumi.enums.Gesture;

public class Player {
    private String name;
    private Gesture currentGesture;
    public final static Player nobody = new Player("Nobody");

    public Gesture getCurrentGesture() {
        return currentGesture;
    }

    public void setGesture(Gesture currentGesture) {
        this.currentGesture = currentGesture;
    }

    public Player(String name) {
        this.name = name;
    }

    public String displayName() {
        if (this.equals(nobody))
            return String.format("Nobody", name);
        else
            return String.format("Player %s", name);
    }

}
