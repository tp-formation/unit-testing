package vince.games.chifoumi.enums;

public enum Gesture {
    stone("Stone"), paper("Paper"), scissors("Scissors");

    public final String label;

    Gesture(String label){
        this.label = label;
    }

}
