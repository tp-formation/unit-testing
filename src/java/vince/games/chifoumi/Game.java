package vince.games.chifoumi;

import vince.games.chifoumi.exceptions.GameEndedException;
import vince.games.chifoumi.exceptions.GameNotEndedException;

import java.util.Stack;

public class Game {
    private final Round[] rounds;
    private final Player player1;
    private final Player player2;
    private int currentRoundNumber = 0;
    private int scorePlayer1 = 0;
    private int scorePlayer2 = 0;
    private final Stack<String> gameRecord = new Stack();

    public Stack<String> getGameRecord() {
        return gameRecord;
    }

    public Game(Player player1, Player player2, int numberOfRounds) {
        this.player1 = player1;
        this.player2 = player2;
        rounds = new Round[numberOfRounds];
    }

    public void playRound() throws GameEndedException, GameNotEndedException {
        if (currentRoundNumber < rounds.length) {
            rounds[currentRoundNumber] = new Round(player1, player2);
            Player winner = rounds[currentRoundNumber].getWinner();
            if (player1.equals(winner)) {
                scorePlayer1 += 1;
                scorePlayer2 -= 1;
            }
            if (player2.equals(winner)) {
                scorePlayer2 += 1;
                scorePlayer1 -= 1;
            }
            currentRoundNumber++;
            gameRecord.add(getCurrentRoundNumber());
            gameRecord.add(getRoundRecord());
            gameRecord.add(getCurrentScore());
            if(currentRoundNumber == rounds.length){
                gameRecord.add(String.format("%s wins", getWinner().displayName()));
            }

        } else throw new GameEndedException();
    }

    public Player getWinner() throws GameNotEndedException {
        if (currentRoundNumber < rounds.length) {
            throw new GameNotEndedException();
        } else if (scorePlayer1 > scorePlayer2) return player1;
        else if (scorePlayer2 > scorePlayer1) return player2;
        else return Player.nobody;

    }

    public String getCurrentScore() {
        return String.format("%s : %d points / %s : %d points",
                player1.displayName(), scorePlayer1,
                player2.displayName(), scorePlayer2);
    }

    public String getCurrentRoundNumber() {
        return String.format("Round %d",
                currentRoundNumber);
    }

    public String getRoundRecord() {
        return String.format("%s : %s / %s : %s",
                player1.displayName(), player1.getCurrentGesture().label,
                player2.displayName(), player2.getCurrentGesture().label);
    }
}